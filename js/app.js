import Vue from "vue";
import { MDCTopAppBar } from "@material/top-app-bar";
import { MDCDialog } from "@material/dialog";
import swal from "sweetalert";
import projects from './projects.json'

var app = new Vue({
  el: "#app",
  data: {
    projects: projects["projects"]
  },
  methods: {
    infomessage() {
      swal({
        title: "About",
        text: `I am Alwin Lohrie aka Niwla23 and I am ${calculateAge(new Date(2006, 5, 23))} years old.
        On this Page you will find some of my Software Projects.`,
        icon: "info",
      });
    },
  },
});

function calculateAge(birthday) { // birthday is a date
  var ageDifMs = Date.now() - birthday;
  var ageDate = new Date(ageDifMs); // miliseconds from epoch
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

window.onload = function () {
  ("use strict");

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js");
  }
};

Vue.prototype.window = window;

const topAppBarElement = document.querySelector(".mdc-top-app-bar");
new MDCTopAppBar(topAppBarElement);
console.log(projects2)