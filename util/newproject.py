import json

with open("../js/projects.json", "r") as file:
    original = json.loads(file.read())


name = input("Name for new project: ")
desc = input("Description for project: ")
image = input(
    "Image Path. You must enclose the url in url(). The default asset path is images/thumbs/image.png: ")
print("Links\nWe will now ask for the Button name and the Link. Once you are done, type 'quit'")
links = {}
while True:
    text = input("Button Text: ")
    if text == "quit":
        break
    link = input("Link: ")
    if link == "quit":
        break
    links[text] = link


article = {
    "name": name,
    "description": desc,
    "startdate": "",
    "image": image,
    "links": {}
}

for key, value in links.items():
    article["links"][key] = value

original["projects"].append(article)

with open("../js/projects.json", "w") as file:
    file.write(json.dumps(original))